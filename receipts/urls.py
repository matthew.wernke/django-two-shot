from django.urls import path
from receipts.views import (
    AccountCreateView,
    AccountListView,
    ExpenseCategoryCreateView,
    ExpenseCategoryListView,
    ReceiptListView,
    ReceiptCreateView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path(
        "categories/",
        ExpenseCategoryListView.as_view(),
        name="show_categories",
    ),
    path("accounts/", AccountListView.as_view(), name="show_accounts"),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="create_category",
    ),
    path(
        "accounts/create/", AccountCreateView.as_view(), name="create_account"
    ),
]
