from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt


class ExpenseCategoryAdmin(ExpenseCategory):
    pass


class AccountAdmin(Account):
    pass


class ReceiptAdmin(Receipt):
    pass


admin.site.register(ExpenseCategory)
admin.site.register(Account)
admin.site.register(Receipt)
