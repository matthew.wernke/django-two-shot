## Feature 1/Setup
* [x] Fork and clone the starter project from django-one-shot
* [x] Create a new virtual environment in the repository directory for the project
* [x] Activate the virtual environment
* [x] Upgrade pip
* [x] Install django
* [x] Install black
* [x] Install flake8
* [x] Install djlint
* [x] Install djhtml
* [x] Deactivate your virtual environment
* [x] Activate your virtual environment
* [x] Use pip freeze to generate a requirements.txt file
  * [x] pip freeze > requirements.txt
* [x] create Django project called "expenses"
* [x] run migrations
* [x] create a superuser
* [x] run tests successfuly
* [x] push to git

## Feature 2
* [x] Create a Django app named "accounts" and install it in the expenses Django project in the INSTALLED_APPS list
* [x] Create a Django app named "receipts" and install it in the expenses Django project in the INSTALLED_APPS list
* [x] Run the migrations
* [x] run tests successfuly
* [x] push to git

## Feature 3
* [x] add models
* [x] run tests successfuly
* [x] push to git

## Feature 4
* [x] register three sites with admin
* [x] view admin to verify
* [x] run tests successfuly
* [x] push to git

## Feature 5
* [ ] Create a view that will get all of the instances of the Receipt model and put them in the context for the template.
* [ ] Register that view in the receipts app for the path "" and the name "home" in a new file named receipts/urls.py.
* [ ] Include the URL patterns from the receipts app in the expenses project with the prefix "receipts/".
* [ ] Create a template for the list view that complies with the following specifications.
* [ ] run tests successfuly
* [ ] push to git

## Feature 6
* [ ]
* [ ] run tests successfuly
* [ ] push to git
